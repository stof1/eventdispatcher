<?php 
namespace DarioRieke\EventDispatcher;

use Psr\EventDispatcher\EventDispatcherInterface as PsrEventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;

/**
 * EventDispatcherInterface
 */
interface EventDispatcherInterface extends PsrEventDispatcherInterface {
	
	/**
	 * add a listener to an event
	 * @param string   $eventName name of the event
	 * @param callable $callback  event callback
	 * @param int      $priority  the priority of the listener, listeners with
	 *                            higher priority values should be called first  
	 */
	public function addListener(string $eventName, callable $callback, int $priority);

	/**
	 * get registered listeners for event
	 * they should be returned in the correct order
	 * @param  string $eventName name of the event
	 * @return iterable          an iterable of callables  
	 */
	public function getListenersForEvent(string $eventName): iterable;

	/**
	 * get all registered listener providers
	 * @return iterable  			array of registered listener providers
	 *                              ListenerProviderInterface[]
	 */
	public function getListenerProviders(): iterable;

	/**
	 * add a listener provider  
	 * @param ListenerProviderInterface $provider
	 */
	public function addListenerProvider(ListenerProviderInterface $provider);

	/**
	 * dispatches an event to all listeners 
	 * 
	 * @param  object    $event      event to dispatch
	 * @param  string    $eventName  the name of the event to dispatch, 
	 *                               if not provided, the event name must be determined by the implementation from the first parameter (event object), usually the class name 
	 * @return object                final event after listeners have processed it      
	 */
	public function dispatch(object $event, string $eventName = null): object;
} 
 ?>