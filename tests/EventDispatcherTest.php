<?php 
declare(strict_types=1);

namespace DarioRieke\EventDispatcher\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\EventDispatcher\EventDispatcher;
use DarioRieke\EventDispatcher\EventDispatcherInterface;
use DarioRieke\EventDispatcher\Tests\Fixtures\TestListenerProvider;
use Psr\EventDispatcher\EventDispatcherInterface as PsrEventDispatcherInterface;
use Psr\EventDispatcher\StoppableEventInterface;
use Psr\EventDispatcher\ListenerProviderInterface;




class EventDispatcherTest extends TestCase {

	const TEST_EVENT = 'Testevent';
	/**
	 * @var EventDispatcher
	 */
	public $dispatcher; 

	
	public function setUp(): void {
		$this->dispatcher = new EventDispatcher();
	}

	public function testImplementsPsr14EventDispatcherInterface() {
		$this->assertInstanceOf(PsrEventDispatcherInterface::class, $this->dispatcher);
	}

	public function testImplementsDarioRiekeEventDispatcherInterface() {
		$this->assertInstanceOf(EventDispatcherInterface::class, $this->dispatcher);
	}

	public function testCanRegisterAndReturnEventListeners() {
		$callable1 = function($event) {

		};
		$callable2 = function($event) {

		};

		$this->dispatcher->addListener(self::TEST_EVENT, $callable1);
		$listeners = $this->dispatcher->getListenersForEvent(self::TEST_EVENT);
		$this->assertCount(1, $listeners);
		$this->assertSame($callable1, $listeners[0]);

		$this->dispatcher->addListener(self::TEST_EVENT, $callable2);
		$listeners = $this->dispatcher->getListenersForEvent(self::TEST_EVENT);
		$this->assertCount(2, $listeners);
		$this->assertSame($callable2, $listeners[1]);

		return $this->dispatcher;
	}

	public function testCanRegisterAndReturnListenerProvider() {
		$provider = $this->getTestListenerProvider();
		$this->dispatcher->addListenerProvider($provider);
		$returnedProviders = $this->dispatcher->getListenerProviders();

		$this->assertSame($provider, $returnedProviders[0]);
		$this->assertCount(1, $returnedProviders);

		return $this->dispatcher;
	}

	/**
	 * @depends testCanRegisterAndReturnListenerProvider
	 */
	public function testCallsListenerProviders() {
		$listenerProvider = $this->getTestListenerProvider();

		$this->dispatcher->addListenerProvider($listenerProvider);
		$this->dispatcher->dispatch($this->getStoppableTestEventMock());

		$this->assertSame(1, $listenerProvider->listener1Counter);
		$this->assertSame(1, $listenerProvider->listener1Counter);

	}

	/**
	 * @depends testCanRegisterAndReturnEventListeners
	 * @dataProvider getTestEvents
	 */
	public function testCallsEventListeners($event) {
		$counter = 0;

		$this->dispatcher->addListener(self::TEST_EVENT, function($recievedEvent) use ($event, &$counter) {
			$counter++;
			$this->assertSame($event, $recievedEvent);
		});

		$counter2 = 0;
		$this->dispatcher->addListener(self::TEST_EVENT, function($recievedEvent) use ($event, &$counter2) {
			$counter2++;
			$this->assertSame($event, $recievedEvent);
		});

		$returnedEvent = $this->dispatcher->dispatch($event, self::TEST_EVENT);

		$this->assertSame($returnedEvent, $event);

		//every method should have been called once
		$this->assertSame(1, $counter);
		$this->assertSame(1, $counter2);
	}

	/**
	 * @depends testCanRegisterAndReturnEventListeners
	 */
	public function testStopsStoppableEvent() {
		$event = $this->getStoppableTestEventMock();

		$this->dispatcher->addListener('stoppableEvent', function($event) {
			$event->stopPropagation();
		});

		$called = false;

		$this->dispatcher->addListener('stoppableEvent', function($event) use (&$called) {
			$called = true;
		});
		$this->dispatcher->addListener('stoppableEvent', function($event) use (&$called) {
			$called = true;
		});

		$this->assertFalse($called);

	}

	
	protected function getStoppableTestEventMock() {
		return $this->createMock(StoppableEventInterface::class);
	}

	protected function getTestListenerProvider() {

		return new class implements ListenerProviderInterface {
			public $listener1Counter = 0;
			public $listener2Counter = 0;

			public function listener1() {
				$this->listener1Counter++;
			}
			public function listener2() {
				$this->listener2Counter++;
			}

	    	public function getListenersForEvent(object $event) : iterable {
	    		return [
	    			[$this, 'listener1'],
	    			[$this, 'listener2']
	    		];
	    	}
	    };
	}

	//data provider
	public function getTestEvents() {
		return [
			'simple object' => [ new \StdClass() ],
			'StoppableEventInterface' => [ $this->getStoppableTestEventMock() ]
		];
	}
}